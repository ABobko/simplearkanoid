package control;

import config.GameConfig;
import model.Ball;
import model.GameMap;
import model.Paddle;
import view.GameDisplay;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.HashMap;

public class GameController {

    private Ball ball;
    private Paddle paddle;
    private int xMapSize;
    private int yMapSize;
    private GameConfig config;
    private GameMap gameMap;
    private Timer timer;
    private boolean play;
    private GameDisplay display;
    private HashMap<Integer, KeyAction> keyDispatcher;

    public GameController(GameDisplay display) {
        config = new GameConfig();
        gameMap = new GameMap(5,8);
        this.display = display;
        timer = new Timer(config.getTimerDelay(), display);
        startTimer();
        this.xMapSize = config.getxMapSize();
        this.yMapSize = config.getyMapSize();
        paddle = new Paddle(300, 100, 20, 15);
        paddle.setyPos(yMapSize - 50);
        ball = new Ball(295, paddle.getyPos() - 30, 15, 0, 0);
        play = false;

        //key Mapping
        keyDispatcher = new HashMap<>();
        keyDispatcher.put(KeyEvent.VK_LEFT, this::moveLeft);
        keyDispatcher.put(KeyEvent.VK_RIGHT, this::moveRight);
        keyDispatcher.put(KeyEvent.VK_SPACE, this::startGame);
    }


    public void drawMap(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, xMapSize, yMapSize);
    }

    public void drawPaddle(Graphics g) {
        g.setColor(Color.BLUE);
        g.fillRect(paddle.getxPos(), paddle.getyPos(), paddle.getWidth(), paddle.getHeight());
    }

    public void drawBall(Graphics g) {
        g.setColor(Color.GREEN);
        g.fillOval(ball.getXPos(), ball.getYPos(),
                ball.getSize(), ball.getSize());
    }

    public void drawBlocks(Graphics g){
        gameMap.drawBlocks(g);
    }

    private void startTimer() {
        timer.start();
    }

    public void moveBall() {
        startTimer();
        ball.move();
        if (ball.getXPos() >= xMapSize || ball.getXPos() < 0) {
            ball.xBounce();
        }
        if (ball.getYPos() < 0) {
            ball.yBounce();
        }

        gameMap.detectCollision(ball, display.getGraphics());

        if (new Rectangle(ball.getXPos(), ball.getYPos(), ball.getSize(), ball.getSize())
                .intersects(new Rectangle(paddle.getxPos(), paddle.getyPos()
                        , paddle.getWidth(), paddle.getHeight()))) {
            ball.yBounce();
        }
        if (ball.getYPos() > yMapSize) {
            gameOver();
        }
        display.repaint();
    }

    public void performKeyAction(KeyEvent e) {
        int keyCode = e.getKeyCode();
        try {
            keyDispatcher.get(keyCode).performAction();
        } catch (NullPointerException ex) {
            //no action taken if key is not mapped
        }
    }

    private void moveLeft() {
        if (paddle.getxPos() - paddle.getWidth() / 2 <= 0) {
            paddle.setxPos(paddle.getWidth() / 2);
        } else {
            paddle.moveLeft();
        }
    }

    private void moveRight() {
        if (paddle.getxPos() >= xMapSize - paddle.getWidth()) {
            paddle.setxPos(xMapSize - paddle.getWidth() / 2);
        } else {
            paddle.moveRight();
        }
    }

    private void startGame() {
        if (!play) {
            play = true;
            ball.setXVelocity(config.getBallXVelocity());
            ball.setYVelocity(config.getBallYVelocity());
        }
    }

    private void gameOver() {
        play = false;
    }

    @FunctionalInterface
    private interface KeyAction {
        void performAction();
    }

}

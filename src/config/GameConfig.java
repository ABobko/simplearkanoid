package config;

public class GameConfig {
    private int xMapSize;
    private int yMapSize;
    private int timerDelay;
    private int ballXVelocity;
    private int ballYVelocity;

    public GameConfig(){
        xMapSize = 600;
        yMapSize = 600;
        timerDelay = 15;
        ballXVelocity = -3;
        ballYVelocity = -6;
    }

    public int getBallXVelocity(){
        return ballXVelocity;
    }

    public int getBallYVelocity(){
        return ballYVelocity;
    }

    public int getxMapSize() {
        return xMapSize;
    }

    public int getyMapSize() {
        return yMapSize;
    }

    public int getTimerDelay() {
        return timerDelay;
    }
}

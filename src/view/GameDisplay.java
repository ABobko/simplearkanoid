package view;

import control.GameController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GameDisplay extends JPanel implements ActionListener, KeyListener {

    private GameController controller;

    GameDisplay(int width, int height) {
        controller = new GameController(this);
        setPreferredSize(new Dimension(width, height));
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);

    }


    public void paint(Graphics g) {
        controller.drawMap(g);
        controller.drawPaddle(g);
        controller.drawBlocks(g);
        controller.drawBall(g);
        g.dispose();
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        controller.moveBall();
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        controller.performKeyAction(e);
    }

}

package view;

import javax.swing.*;

public class PrimaryWindow extends JFrame {

    public PrimaryWindow(int width, int height){
        super();
        setTitle("Simple Arkanoid");
        GameDisplay screen = new GameDisplay(width, height);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add(screen);
        setVisible(true);
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
        repaint();
    }
}

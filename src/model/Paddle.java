package model;

public class Paddle {
    private int xPos;
    private int width;
    private int height;
    private int velocity;
    private int yPos;

    public Paddle(){}

    public Paddle(int xPos, int width, int height, int velocity){
        this.xPos = xPos;
        this.width = width;
        this.height = height;
        this.velocity = velocity;

    }

    public void moveRight(){
        xPos += velocity;
    }
    public void moveLeft(){
        xPos -= velocity;
    }

    public int getxPos() {
        return xPos-width/2;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getVelocity(){
        return velocity;
    }

    public void setxPos(int xPos){
        this.xPos = xPos;
    }

    public int getyPos(){
        return yPos;
    }

    public void setyPos(int yPos){
        this.yPos = yPos;
    }
}

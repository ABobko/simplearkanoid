package model;

public class Ball {
    private int xPos;
    private int yPos;
    private int xVelocity;
    private int yVelocity;
    private int size;


    public Ball(){
    }

    public Ball(int xPos, int yPos, int size, int xVelocity, int yVelocity){
        this.size = size;
        this.xPos = xPos;
        this.yPos = yPos;
        this.size = size;
        this.xVelocity = xVelocity;
        this.yVelocity = yVelocity;
    }


    public void setXVelocity(int xVelocity) {
        this.xVelocity = xVelocity;
    }

    public void setYVelocity(int yVelocity){
        this.yVelocity = yVelocity;
    }

    public void move(){
        xPos += xVelocity;
        yPos += yVelocity;
    }

    public int getXPos(){
        return xPos;
    }

    public int getYPos(){
        return yPos;
    }

    public int getxVelocity() {
        return xVelocity;
    }

    public int getyVelocity() {
        return yVelocity;
    }

    public int getSize() {
        return size;
    }

    public void xBounce(){
        xVelocity = -xVelocity;
    }
    public void yBounce(){
        yVelocity = -yVelocity;
    }
    public void setYPos(int yPos){
        this.yPos = yPos;
    }

}

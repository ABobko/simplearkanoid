package model;

import java.awt.*;
import java.util.HashSet;
import java.util.LinkedList;

public class GameMap {
    private int rows;
    private int columns;
    private Block map[][];
    private int blocksTotal;
    private HashSet<Block> blockSet;

    public GameMap(int rows, int columns) {
        createBlocks(rows, columns);
        this.rows = rows;
        this.columns = columns;
        blocksTotal = rows * columns;
    }

    private void createBlocks(int rows, int columns) {
        map = new Block[rows][columns];
        blockSet = new HashSet<>();
        int blockWidth = 500 / columns;
        int blockHeight = 200 / rows;
        for (int j = 0; j < rows; j++) {
            for (int k = 0; k < columns; k++) {
                map[j][k] = new Block(k * blockWidth + 50, j * blockHeight + 50,
                        blockWidth, blockHeight);
                blockSet.add(map[j][k]);
            }
        }
    }

    public void drawBlocks(Graphics g) {
      blockSet.stream()
              .filter(block->!block.wasHit())
              .forEach(block -> drawSingleBlock(block,g));
    }

    private void drawSingleBlock(Block block, Graphics g) {
        if (!block.wasHit()) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(Color.ORANGE);
            g2.fillRect(block.getxPos(),
                    block.getyPos(),
                    block.getWidth(),
                    block.getHeight());
            g2.setColor(Color.BLACK);
            g2.setStroke(new BasicStroke(3));
            g2.drawRect(block.getxPos(),
                    block.getyPos(),
                    block.getWidth(),
                    block.getHeight());
        }
    }


    public void detectCollision(Ball ball, Graphics g) {
        blockSet.stream()
                .filter(block -> !block.wasHit())
                .forEach(block ->checkIntersection(block, ball));
    }

    private void checkIntersection(Block block, Ball ball) {
        int brickX = block.getxPos();
        int brickY = block.getyPos();
        int brickWidth = block.getWidth();
        int brickHeight = block.getHeight();
        int ballX = ball.getXPos();
        int ballY = ball.getYPos();
        int ballSize = ball.getSize();

        Rectangle blockRect = new Rectangle(brickX, brickY,
                brickWidth, brickHeight);
        Rectangle ballRect = new Rectangle(ballX, ballY,
                ball.getSize(), ball.getSize());

        if (blockRect.intersects(ballRect)) {
            block.hitBlock();

            if(ballX +ballSize <= brickX || ballX>= blockRect.x +blockRect.width){
                ball.xBounce();
            } else {
                ball.yBounce();
            }
        }
    }

}
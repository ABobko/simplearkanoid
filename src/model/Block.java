package model;

public class Block {
    private int xPos;
    private int yPos;
    private int width;
    private int height;
    private boolean wasHit;

    public Block(int xPos, int yPos, int width, int height){
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
        wasHit = false;
    }

    public boolean wasHit(){
        return wasHit;
    }

    public void hitBlock(){
        wasHit = true;
    }

    int getxPos(){
        return xPos;
    }
    int getyPos(){
        return yPos;
    }

    int getWidth(){
        return width;
    }
    int getHeight(){
        return height;
    }

}
